package com.trainer.pokemon.team;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainerPokemonTeamApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainerPokemonTeamApplication.class, args);
	}

}
